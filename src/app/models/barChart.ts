export interface BarChartDataModel {
  data: [number, number, number, number, number, number, number],
  label;
}
