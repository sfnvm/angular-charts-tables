import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './view/home/home.component';

import { TablesComponent } from './view/tables/tables.component';
import { PTableComponent } from './view/tables/p-table/p-table.component';
import { MatTableComponent } from './view/tables/mat-table/mat-table.component';

import { ChartsComponent } from './view/charts/charts.component';
import { BarChartComponent } from './view/charts/bar-chart/bar-chart.component';
import { DoughnutChartComponent } from './view/charts/doughnut-chart/doughnut-chart.component';
import { RadarChartComponent } from './view/charts/radar-chart/radar-chart.component';
import { PieChartComponent } from './view/charts/pie-chart/pie-chart.component';
import { TestTableComponent } from './view/tables/test-table/test-table.component';

const routes: Routes = [
  { path: '', component: HomeComponent, pathMatch: 'full' },
  { path: 'table', component: TablesComponent },
  { path: 'table/p-table', component: PTableComponent },
  { path: 'table/mat-table', component: MatTableComponent },
  { path: 'table/test-table', component: TestTableComponent },
  { path: 'chart', component: ChartsComponent },
  { path: 'chart/bar-chart', component: BarChartComponent },
  { path: 'chart/doughnut-chart', component: DoughnutChartComponent },
  { path: 'chart/radar-chart', component: RadarChartComponent },
  { path: 'chart/pie-chart', component: PieChartComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
