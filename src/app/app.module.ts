import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import '../polyfills';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatNativeDateModule } from '@angular/material';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Material Modules
import { MaterialModule } from './material-modules';

// ChartsJS Modules
import { ChartsModule } from 'ng2-charts';

// PrimeNG Modules
import { PrimeNGModule } from './shared/imports/primeng-modules';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavComponent } from './view/nav/nav.component';
import { HomeComponent } from './view/home/home.component';
// Tables Import
import { TablesComponent } from './view/tables/tables.component';
import { MatTableComponent } from './view/tables/mat-table/mat-table.component';
import { PTableComponent } from './view/tables/p-table/p-table.component';
import { TestTableComponent } from './view/tables/test-table/test-table.component';
// Charts Import
import { ChartsComponent } from './view/charts/charts.component';
import { BarChartComponent } from './view/charts/bar-chart/bar-chart.component';
import { DoughnutChartComponent } from './view/charts/doughnut-chart/doughnut-chart.component';
import { RadarChartComponent } from './view/charts/radar-chart/radar-chart.component';
import { PieChartComponent } from './view/charts/pie-chart/pie-chart.component';
import { SideNavComponent } from './view/nav/side-nav/side-nav.component';

import { EditableComponent } from './shared/editable/editable.component';
import { ViewModeDirective } from './shared/editable/view-mode.directive';
import { EditModeDirective } from './shared/editable/edit-mode.directive';
import { EditableOnEnterDirective } from './shared/editable/editable-on-enter.directive';
import { FocusableDirective } from './focusable.directive';

import { ToastModule } from 'primeng/toast';
import { MessageService } from 'primeng/primeng';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    HomeComponent,
    // Table
    TablesComponent,
    PTableComponent,
    MatTableComponent,
    TestTableComponent,
    // Chart
    ChartsComponent,
    BarChartComponent,
    DoughnutChartComponent,
    RadarChartComponent,
    PieChartComponent,
    SideNavComponent,
    // Directive
    EditableComponent,
    ViewModeDirective,
    EditModeDirective,
    EditableOnEnterDirective,
    FocusableDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    MaterialModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    ChartsModule,
    PrimeNGModule,
    ToastModule
  ],
  providers: [
    MessageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

platformBrowserDynamic().bootstrapModule(AppModule);
