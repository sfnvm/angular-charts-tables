import { Component, OnInit } from '@angular/core';
import { TestTableService } from './test-table.service';
import { Car } from 'src/domain/car';
import { FormGroup, FormControl, FormArray } from '@angular/forms';
import { SelectItem } from 'primeng/api';
import { MessageService } from 'primeng/primeng';

@Component({
  selector: 'app-test-table',
  templateUrl: './test-table.component.html',
  styleUrls: ['./test-table.component.css']
})
export class TestTableComponent implements OnInit {

  cols: any[];
  car: Car;
  cars: Car[];

  vins: SelectItem[];
  years: SelectItem[];
  brands: SelectItem[];
  colors: SelectItem[];

  carsChanged: Car[];

  controls: FormArray;

  rowNum = Math.random() * 10;

  abc = true;

  constructor(private carService: TestTableService,
    // tslint:disable-next-line: align
    private messageService: MessageService) { }

  ngOnInit() {
    this.carService.getCarsSmall()
      .then(cars => {
        this.cars = cars;
        console.log(this.cars);

        this.vins = this.cars.map(car => {
          return { label: car.vin, value: car.vin };
        });

        this.years = this.cars.map(car => {
          return { label: car.year, value: car.year };
        });

        this.brands = this.cars.map(car => {
          return { label: car.brand, value: car.brand };
        });

        this.colors = this.cars.map(car => {
          return { label: car.color, value: car.color };
        });

        const toGroups = this.cars.map(car => {
          return new FormGroup({
            vin: new FormControl(car.vin),
            year: new FormControl(car.year),
            brand: new FormControl(car.brand),
            color: new FormControl(car.color)
          });
        });
        this.controls = new FormArray(toGroups);
      });
    this.cols = [
      { field: 'vin', header: 'Vin' },
      { field: 'year', header: 'Year' },
      { field: 'brand', header: 'Brand' },
      { field: 'color', header: 'Color' }
    ];
  }

  getControl(id: number, field: string) {
    // console.log(id);
    // console.log(field);
    // console.log(this.controls.at(id).get(field));
    return this.controls.at(id).get(field) as FormControl;
  }

  updateField(index: number, field: string) {
    const control = this.getControl(index, field);
    if (control.valid) {
      if (control.dirty) {
        console.log('edited cell!');
      }
      control.valueChanges.subscribe(() => {
        console.log('abc');
      });
      this.cars = this.cars.map((e, i) => {
        if (index === i) {
          return {
            ...e,
            [field]: control.value
          };
        }
        return e;
      });
    }
  }

  cloneCar(c: Car) {
    const car = {} as Car;
    for (const prop in c) {
      if (true) {
        car[prop] = c[prop];
      }
    }
    return car;
  }

  add() {
    console.log('add works!');
  }

  delete() {
    console.log('delete works!');
  }

  save() {
    console.log('save works!');
  }

  cancel() {
    console.log('cancel works!');
  }

  cellUpdated() {
    console.log('this cell is updated!');
  }

}
