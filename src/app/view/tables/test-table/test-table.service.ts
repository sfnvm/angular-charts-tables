import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Car } from 'src/domain/car';

@Injectable({
  providedIn: 'root'
})
export class TestTableService {

  constructor(private http: HttpClient) { }

  getCarsSmall() {
    return this.http.get<any>('assets/data/cars-medium.json')
      .toPromise()
      .then(res => res.data as Car[])
      .then(data => data);
  }
}
