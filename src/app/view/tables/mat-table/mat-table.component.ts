import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { FormControl } from '@angular/forms';

export interface PeriodicElement {
  name: string;
  id: number;
  colour: string;
  pet: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  { name: 'John', id: 1, colour: 'Green', pet: 'Dog' },
  { name: 'Sarah', id: 2, colour: 'Purple', pet: 'Cat' },
  { name: 'Lindsay', id: 3, colour: 'Blue', pet: 'Lizard' },
  { name: 'Megan', id: 4, colour: 'Orange', pet: 'Dog' },
  { name: 'John', id: 1, colour: 'Green', pet: 'Dog' },
  { name: 'Sarah', id: 2, colour: 'Purple', pet: 'Cat' },
  { name: 'Lindsay', id: 3, colour: 'Blue', pet: 'Lizard' },
  { name: 'Megan', id: 4, colour: 'Orange', pet: 'Dog' }
];

@Component({
  selector: 'app-mat-table',
  templateUrl: './mat-table.component.html',
  styleUrls: ['./mat-table.component.css']
})
export class MatTableComponent implements OnInit {

  columnsToDisplay: string[] = ['name', 'id', 'favouriteColour', 'pet'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);

  nameFilter = new FormControl('');
  idFilter = new FormControl('');
  colourFilter = new FormControl('');
  petFilter = new FormControl('');

  filterValues = {
    name: '',
    id: '',
    colour: '',
    pet: ''
  };

  constructor() {
    this.dataSource.filterPredicate = this.tableFilter();
  }

  ngOnInit() {
    this.nameFilter.valueChanges
      .subscribe(
        name => {
          this.filterValues.name = name;
          this.dataSource.filter = JSON.stringify(this.filterValues);
        }
      );
    this.idFilter.valueChanges
      .subscribe(
        id => {
          this.filterValues.id = id;
          this.dataSource.filter = JSON.stringify(this.filterValues);
        }
      )
    this.colourFilter.valueChanges
      .subscribe(
        colour => {
          this.filterValues.colour = colour;
          this.dataSource.filter = JSON.stringify(this.filterValues);
        }
      );
    this.petFilter.valueChanges
      .subscribe(
        pet => {
          this.filterValues.pet = pet;
          this.dataSource.filter = JSON.stringify(this.filterValues);
        }
      );
  }

  tableFilter(): (data: any, filter: string) => boolean {
// tslint:disable-next-line: prefer-const
    let filterFunction = (data, filter): boolean => {
// tslint:disable-next-line: prefer-const
      let searchTerms = JSON.parse(filter);
      return data.name.toLowerCase().indexOf(searchTerms.name) !== -1
        && data.id.toString().toLowerCase().indexOf(searchTerms.id) !== -1
        && data.colour.toLowerCase().indexOf(searchTerms.colour) !== -1
        && data.pet.toLowerCase().indexOf(searchTerms.pet) !== -1;
    };
    return filterFunction;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
