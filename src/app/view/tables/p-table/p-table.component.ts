import { Component, OnInit, ViewChild } from '@angular/core';

import { LazyLoadEvent, DataTable } from 'primeng/primeng';

import { BarChartService } from 'src/app/services/bar-chart.service';
import { BarChartDataModel } from 'src/app/models/barChart';

@Component({
  selector: 'app-p-table',
  templateUrl: './p-table.component.html',
  styleUrls: ['./p-table.component.css']
})
export class PTableComponent implements OnInit {
  @ViewChild('dt') public dataTable: DataTable;

  displayDialog: boolean;
  barData: BarChartDataModel[];
  newData: boolean;
  cols: any[];
  singleBarData = {} as BarChartDataModel;

  constructor(private barChartService: BarChartService) { }

  ngOnInit() {

    this.barChartService.getBarChartData()
      .then(barData => {
        console.log(barData);
        this.barData = barData;
      });

    this.cols = [
      { field: 'label', header: 'Label' },
      { field: 'data[0]', header: '2006' },
      { field: 'data[1]', header: '2007' },
      { field: 'data[2]', header: '2008' },
      { field: 'data[3]', header: '2009' },
      { field: 'data[4]', header: '2010' },
      { field: 'data[5]', header: '2011' },
      { field: 'data[6]', header: '2012' }
    ];
  }

  showDialogToAdd() {
    this.newData = true;
    this.singleBarData = {} as BarChartDataModel;
    this.displayDialog = true;
  }

}
