import { NgModule } from '@angular/core';

import { FormsModule, ReactiveFormsModule, FormArray } from '@angular/forms';

@NgModule({
  exports: [
    FormsModule,
    ReactiveFormsModule,
    FormArray
  ]
})

export class AngularModules { }
