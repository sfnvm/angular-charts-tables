import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';

import { BarChartDataModel } from '../models/barChart';

@Injectable({
  providedIn: 'root'
})
export class BarChartService {

  constructor(private http: HttpClient) { }

  getBarChartData() {
    return this.http.get<any>('assets/data/bar-chart.json')
          .toPromise()
          .then(res => res.data as BarChartDataModel[])
          .then(data => data);
  }

}
